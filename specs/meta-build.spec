%global app                     build
%global d_bin                   %{_bindir}

Name:                           meta-build
Version:                        1.0.3
Release:                        2%{?dist}
Summary:                        META-package for install and configure build system
License:                        GPLv3

Source10:                       run.%{app}.rpm.sh
Source11:                       run.%{app}.spec.sh

Requires:                       git mock meta-system

%description
META-package for install and configure build system.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0755 %{SOURCE10} \
  %{buildroot}%{d_bin}/run.%{app}.rpm.sh
%{__install} -Dp -m 0755 %{SOURCE11} \
  %{buildroot}%{d_bin}/run.%{app}.spec.sh


%files
%{d_bin}/run.%{app}.rpm.sh
%{d_bin}/run.%{app}.spec.sh


%changelog
* Wed Oct 16 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-2
- NEW: SPEC script.

* Mon Oct 07 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-1
- NEW: SPEC script.

* Wed Oct 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.2-1
- UPD: Build script.

* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-7
- UPD: Build script.

* Wed Jul 31 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-6
- UPD: SPEC-file.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-5
- UPD: Build script.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-4
- UPD: SPEC-file.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-3
- UPD: Build script.
- UPD: SPEC-file.

* Mon Jul 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-2
- UPD: MARKETPLACE.

* Sun Jun 23 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-1
- UPD: Build script.

* Sat Jun 22 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-0
- UPD: Build script.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- UPD: Build script.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
